.ONESHELL:

DOCKER ?= docker
REGISTRY ?= registry.core.hpc.ethz.ch/pipeline

kube-linter-build:
	cd images
	$(DOCKER) build . -t kube-linter:latest -f Dockerfile.kube-linter

kube-linter-push:
	$(DOCKER) tag kube-linter:latest $(REGISTRY)/kube-linter:latest
	$(DOCKER) push $(REGISTRY)/kube-linter:latest

yamllint-build:
	cd images
	$(DOCKER) build . -t yamllint:latest -f Dockerfile.yamllint

yamllint-push:
	$(DOCKER) tag yamllint:latest $(REGISTRY)/yamllint:latest
	$(DOCKER) push $(REGISTRY)/yamllint:latest

shellcheck-build:
	cd images
	$(DOCKER) build . -t shellcheck:latest -f Dockerfile.shellcheck

shellcheck-push:
	$(DOCKER) tag shellcheck:latest $(REGISTRY)/shellcheck:latest
	$(DOCKER) push $(REGISTRY)/shellcheck:latest

trivy-build:
	cd images
	$(DOCKER) build . -t trivy:latest -f Dockerfile.trivy

trivy-push:
	$(DOCKER) tag trivy:latest $(REGISTRY)/trivy:latest
	$(DOCKER) push $(REGISTRY)/trivy:latest


lint:
	yamllint -s .
