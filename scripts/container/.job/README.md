# Internal jobs

Jobs files located here are meant only for internal job instantiation.

The main purpose of these jobs is to allow for triggering of job templates from
other job templates via: `job.trigger.include`. For example:

```yaml
# meant to be instantiated by a user job
.trigger-job-template:
  stage: build
  trigger:
    include:
      # trigger child jobs from this directory, which must define jobs, not templates
      - local: scripts/container/.job/downstream-job-a.yaml
      - local: scripts/container/.job/downstream-job-b.yaml
```
