# Pipelines

This repository contains all our pipelines.

# Warning
Do not push anything to gitlab.com, use our own private repository.
If you wish to contribute and don't have access to our repository, please open an issue and we will find a solution.

# Adding a Pipeline

You will need to create the file `.gitlab-ci.yml` in your repository. Here is an example:

```yaml
stages:
  - renovate   # only when including scripts/renovate.yaml
  - test
  - sast
  - build      # build and push containers

include:
  - project: "hpc/pipelines"
  # ref: "main"
    file:
      - "scripts/chart/kube-linter.yaml"
      - "scripts/renovate.yaml"
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

chart-kube-linter:
  extends: .kube-linter
  rules:
    - if: >-
        $CI_PIPELINE_SOURCE !~ /^(?:push|merge_request_event|schedule)$/ &&
        $RENOVATE == "true"
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - chart/*
      when: always
  variables:
    CHART_DIRECTORY: chart/

variables:
  GIT_SUBMODULE_STRATEGY: recursive
```

## Pipelines Currently Available

* Python
  * Format (black, isort, flake8)
  * Test (pytest [coverage report], bandit and safety)
* Javascript (npm based, depends on packages.json)
  * Format
  * Test (coverage report)
* Shell
  * Check (shellchecks)
  * Format (shfmt)
* Containers
  * Build / push (docker / kaniko)
  * Trivy (security vulnerabilities)
  * Hadolint (best practices in dockerfile)
* K8s
  * Linter (kube-linter)
  * Publish on gitlab
* YAML
  * Linter (yamllint)

# Common Tricks to Use our Pipelines
Here you will find a few tricks that we are regularly using when calling our pipelines.

## Preparing the environment
Often the environment needs to be prepared by the user. It can be done with `before_script`:
```yaml
python-test:
  extends: .python-test
  before_script:
    - pip3 install poetry
    - poetry install
    - source "$( poetry env list --full-path | grep Activated | cut -d' ' -f1 )/bin/activate"
```

## Using Renovate

For general info and tips on getting your repository renovated, check out
[hpc/charts/renovate](https://gitlab.ethz.ch/hpc-internal/charts/renovate/-/tree/main/runner?ref_type=heads).

This repo provides two useful additions:

- [renovate-config-lint job](scripts/renovate/config-lint.yaml) to validate
  `.gitlab/renovate.json5` in your repository if it exists.
- [renovate-run job](scripts/renovate/run.yaml) to enable manual trigger of
  renovate against your repository

For convenience, [scripts/renovate.yaml](scripts/renovate.yaml) has been provided to enable both of them with a single include.

To use them, make sure to add `renovate` stage in your `.gitlab-ci.yml`:

```yaml
---
stages:
  - lint
  - renovate

include:
  - project: hpc/pipelines
    file:
      - scripts/renovate.yaml
```

To trigger the renovate job, follow [Run a pipeline
manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) and
make sure to set `RENOVATE=true` like this:

![](./docs/pics/run-renovate-pipeline.png)

### Preventing other jobs from running in the renovate pipeline

The default job rules will add them to the renovate pipeline even though the
only job you'd like to run is `renovate`.

To prevent linter, tester or builder jobs to run, make sure to add the rule
like this:

```yaml
build:
  extends: .build-push-check
  variables:
    DOCKERFILE: images/Dockerfile
  rules:
    # do not run in the renovate pipeline
    # NOTE: the first matching rule applies, the rest of the rules is ignored
    - if: >-
        $CI_PIPELINE_SOURCE !~ /^(?:push|merge_request_event|schedule)$/ &&
        $RENOVATE == "true"
      when: never
    # run in all other cases as long as the previous stages succeed
    - when: on_success
```

## Managing Sub-directories
If you wish to apply a pipeline to a sub-directory, then you can simply use `before_script` and `after_script`:
```yaml
python-test:
  extends: .python-test
  before_script:
    - cd app
  after_script:
    - cp coverage.xml ../
```

## Special Behavior

We often use environment variables to define special behaviors (e.g. arguments to a software, path, ...):
```yaml
python-test:
  extends: .python-test
  variables:
    PYTEST_ARGS: -k my_favourite_test
```

# Stages

We currently only consider the following stages:

```yaml
stages:
 - renovate  # only when using scripts/renovate.yaml
 - lint
 - test
 - build
```
